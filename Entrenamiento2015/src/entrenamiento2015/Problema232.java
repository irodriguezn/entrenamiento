/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entrenamiento2015;

import java.util.Scanner;

/**
 *
 * @author nachorod
 */
public class Problema232 {
    public static void main(String[] args) {
        // Esto siempre lo primero de todo:
        Scanner sc = new Scanner(System.in);
        
        // Aquí todas las variables que se vayan a utilizar:
        int nacimientoDia, nacimientoMes, nacimientoAño;
        int noCumpleDia, noCumpleMes, noCumpleAño;
        int añosCompletos=0;
        int diasMeses, diasTotales=0;

        // Código para leer cada caso de prueba:
        nacimientoDia=sc.nextInt();
        while (nacimientoDia!=0) {
            nacimientoMes=sc.nextInt();
            nacimientoAño=sc.nextInt();
            noCumpleDia=sc.nextInt();
            noCumpleMes=sc.nextInt();
            noCumpleAño=sc.nextInt();
            
            // Código para resolver el problema
            // e imprimir la salida
            // Si es el día de mi cumpleaños devolvemos cero
            if (nacimientoDia==noCumpleDia && nacimientoMes==noCumpleMes) {
                System.out.println(0);
            } else {
                if (noCumpleAño!=nacimientoAño) {
                    if (noCumpleMes>nacimientoMes || noCumpleDia>nacimientoDia) {
                        añosCompletos=noCumpleAño-nacimientoAño-1;
                    } else {
                        añosCompletos=noCumpleAño-nacimientoAño;                        
                    }
                }
                
                if (nacimientoMes==noCumpleMes) {
                    diasMeses=0;
                } else {
                    diasMeses=calcularDiasMes(nacimientoMes, noCumpleMes);
                }
                
                diasTotales=añosCompletos*364+diasMeses+Math.abs(noCumpleDia-nacimientoDia);
            }
            System.out.println(diasTotales);
            // Fin código
            
            // Condición para leer siguiente caso o salir:
            nacimientoDia=sc.nextInt();
        }
        
    } // fin main
    
    // Funciones estáticas que necesite el programa:
    static int numDiasMes(int mes) {
        int dias=0;
        switch (mes) {
            case 1:
                dias=31;
                break;
            case 2:
                dias=28;
                break;
            case 3:
                dias=31;
                break;
            case 4:
                dias=30;
                break;
            case 5:
                dias=31;
                break;
            case 6:
                dias=30;
                break;                
            case 7:
                dias=31;
                break;
            case 8:
                dias=31;
                break;
            case 9:
                dias=30;
                break;
            case 10:
                dias=31;
                break;
            case 11:
                dias=30;
                break;
            case 12:
                dias=31;
                break;
        }
        return dias;
    }
    
    static int calcularDiasMes(int mesInicio, int mesFin) {
        int dias=0;
        if (mesInicio<mesFin) {
            for (int i=mesInicio; i<mesFin; i++) {
                dias+=numDiasMes(i);
            }
        } else {
            // mesInicio=5 mesFin=3
            for (int i=mesInicio; i<=12; i++) {
                dias+=numDiasMes(i);
            }
            
            for (int i=1; i<mesFin; i++) {
                dias+=numDiasMes(i);
            }
        }
        
        return dias;
    }
    
}
